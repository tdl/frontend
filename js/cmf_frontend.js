$(document).ready(function() {
    $('.btn[data-toggle="button"]').click(function() {
        if ($(this).attr('class-toggle') != undefined && !$(this).hasClass('disabled')) {
            if ($(this).attr('data-toggle') == 'button') {
                if ($(this).hasClass('active')) {
                    $(this).removeClass($(this).attr('class-toggle'));
                    $(this).text("Просмотр");
                    $(".node").removeClass("node-edit");
                } else {
                    $(this).addClass($(this).attr('class-toggle'));
                    $(this).text("Редактирование");
                    $(".node").addClass("node-edit");
					$('.node-edit').hover(
					  function(){
						var elem = this;
						$.get('overlay_content.html', function(data){
						$(elem).prepend(data);
						$('.popup-trigger').click(function() {
						  $.SC_Overlay('open');
						  return false;
						});
					  });
					  }, 
					  function(){
						var elem = this;
						$(elem).find('.overlay.btn-group').hide().remove();
					  }
					);

                }
            }
        }
    });
    $('#pop1').popover(html=true)
    $('#pop2').popover()
});
